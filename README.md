Cloned from [RASflow](https://github.com/zhxiaokang/RASflow)

In order to keep the original repository as light as possible, only the example data and its output are kept there.

This repository is to store the output from testing RASflow on some real datasets.

It has currently been tested on four datasets: 
1. (human) pair-end RNA-seq of prostate cancer and adjacent normal tissues from 14 patients (ArrayExpress accession: E-MTAB-567) [1]
2. (mouse) single-end RNA-Seq of mesenchymal stem cells (MSCs) and cancer-associated fibroblasts (CAFs) from EG7 tumor bearing mice (GEO accession: GSE141199)
3. (cod) pair-end RNA-Seq of Atlantic cod liver slices exposed to benzo[a]pyrene (BaP) and 17α-ethynylestradiol (EE2) (GEO accession: GSE106968) [2]
4. (benchmark) single-end RNA-Seq of highly purified human classical and nonclassical monocyte subsets from a clinical cohort (SRA accession: SRP082682) [3]

RASflow was run on four real datasets using a 1TB RAM 60 cores Dell PowerEdge R910 machine. 
RASflow was also tested on the mouse dataset using Windows Subsystem for Linux on a 8GB RAM 4 cores Intel Core 2 machine (branch "mouse_pc").

Please switch the branch to check each of them.

**Reference**

[1] Ren, S., Peng, Z., Mao, J.H., Yu, Y., Yin, C., Gao, X., Cui, Z., Zhang, J., Yi, K., Xu, W., Chen, C., Wang, F., Guo, X., Lu, J., Yang, J., Wei, M., Tian, Z., Guan, Y., Tang, L., Xu, C., Wang, L., Gao, X., Tian, W., Wang, J., Yang, H., Wang, J., Sun, Y.: RNA-seq analysis of prostate cancer in the Chinese population identifies recurrent gene fusions, cancer-associated long noncoding RNAs and aberrant alternative splicings. Cell Research 22(5), 806–821 (2012)

[2] Yadetie, F., Zhang, X., Hanna, E.M., Aranguren-Abad´ıa, L., Eide, M., Blaser, N., Brun, M., Jonassen, I., Goksøyr, A., Karlsen, O.A.: Rna-seq analysis of transcriptome responses in atlantic cod (gadus morhua) precision-cut liver slices exposed to benzo [a] pyrene and 17α-ethynylestradiol. Aquatic toxicology 201, 174–186 (2018)

[3] C. R. Williams, A. Baccarella, J. Z. Parrish, and C. C. Kim, “Empirical assessment of analysis workflows for differential expression analysis of human samples using RNA-Seq,” BMC Bioinformatics, vol. 18, no. 1 (2017)